const Category = require("./category.model");

class CategoryResolver {
    static async findByWine(type) {
        return Category.find({ wines: type });
    }
}

module.exports = CategoryResolver;