const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const category = {
    type: { type: String, required: true},
    wines: [{ type: mongoose.Types.ObjectId, ref: 'Wine', required: true}]

}

const categorySchema = new Schema(category, { timestamps: true })

const Category = mongoose.model('Category', categorySchema);
module.exports = Category;