const WinesResolver = require("../wines/wines.resolver");
const StatusError = require("../_shared/error/status.error");
const Category = require("./category.model");

class CategoryService {

    static find(extended) {
        const find = Category.find();
        return extended ? find.populate('wines') : find;
    }

    static async findOne(id) {
        const category = await Category.findById(id);

        if (category) {
            return category;
        }

        throw new StatusError(404, `Category with id <${id}> was not found`);
    }

    static async create(category) {
        await WinesResolver.winesExistsById(category.wines)

    
        return Category.create(category);
    }

    static async replace(id, category) {
        const updated = await Category.findByIdAndUpdate(id, category);

        if (updated) {
            return updated;
        }

        throw new StatusError(404, `Category with id <${id}> was not found`);
    }

    static async delete(id) {
        const category = await Category.findById(id);


        if (category) {
            return Category.findByIdAndRemove(id);
        }

        throw new StatusError(404, `Category with id <${id}> was not found`);
    }
}

module.exports = CategoryService;