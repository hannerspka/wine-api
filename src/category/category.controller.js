const express = require('express');
const CategoryService = require('./category.service');
const CategoryController = express.Router();


CategoryController.get('/', async (req, res, next) => {
        try {
            const { extended } = req.query;
            const categories = await CategoryService.find(extended);
            res.json(categories);
        } catch (error) {
            next(error);
        }
});

CategoryController.get('/:id',async (req, res, next) => {
    try {
        const { id } = req.params;

        const category = await CategoryService.findOne(id);

        res.json(category);
    } catch (error) {
        next(error);
    }
});

CategoryController.post('/', async (req, res, next) => {
        try{
            const { type, wines } = req.body;
            const created = await CategoryService.create({ type, wines });
        res.status(201).json(created);
    } catch (error) {
        next(error);
    } 
})

CategoryController.put('/:id', async (req, res, next) => {
   try {
        const { type } = req.body;
        const { id } = req.params;
        const updated = await CategoryService.replace(id, { type });
        res.json(updated);
    } catch (error) {
       next(error);
    }
})

CategoryController.delete('/:id', async (req, res, next) => {
    try {
        const { id } = req.params;

        await CategoryService.delete(id);

       res.status(204).send();
   } catch (error) {
        next(error);
    }
})


module.exports = CategoryController;