const express = require("express");
const mongoose = require("mongoose");
const WinesController = require("./wines/wines.controller");
const CategoryController = require("./category/category.controller");
const { defaults } = require("./_shared/utils");
//const cloudinary = require('cloudinary').v2;
const app = express();

require("dotenv").config();

const PORT = defaults(process.env.PORT, 3000);

const loggingMiddleware = (req, res, next) => {
  console.log(`[${req.method}] ${req.originalUrl}`);
  next();
};

app.use(express.json());
app.use(loggingMiddleware);

app.use("/wines", WinesController);
app.use("/category", CategoryController);
app.use("*", (req, res) => res.status(401).json("Path not existing"));

app.use((error, req, res, next) => {
  const exception = {
    status: defaults(error.status, 500),
    message: defaults(error.message, "An unexpected error happened"),
  };

  if (process.env.NODE_ENV !== "production") {
    exception["callstack"] = error.stack;
  }

  console.error(exception);
  res.status(exception.status).json(exception);
});

mongoose
  .connect(process.env.MONGO_URI,
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(() => {
    app.listen(PORT, () =>
      console.info(`Server is running in http://localhost:${PORT}`)
    );
  });

//cloudinary.config({
// cloud_name: process.env.CLOUD_NAME,
// api_key: process.env.API_KEY,
//  api_secret: process.env.API_SECRET
//});
