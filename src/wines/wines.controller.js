const express = require('express');
const WinesController = express.Router();
const WineService = require('./wines.service');


WinesController.get('/', async (req, res, next) => {
    try{
        const wines = await WineService.find();
        res.json(wines);
    }catch (error) {
        next(error)
    }   
});

WinesController.get('/:id', async (req, res, next) => {
    try{
        const { id } = req.params;
        const wine = await WineService.findOne(id)
    
        res.json(wine);
    }catch (error) {
        next(error);
    }
});

WinesController.post('/', async (req, res, next) => {
    try{
        const { wine, type, year, winery, region, country } = req.body;
        const created = await WineService.create({wine, type, year, winery, region, country});
        res.status(201).json(created);

    }catch (error){
        next(error);
    }
   
});

WinesController.put('/:id', async (req, res) => {
    try{
        const { wine, type, year } = req.body;
        const { id } = req.params;

        const updateWine = await WineService.replace(id, {wine, type, year});
        res.json(updateWine);
    }catch (error){
        next(error);
    }
});

WinesController.delete('/:id', async (req, res, next) => {
    try{
        const { id } = req.params;
        await WineService.delete(id);
        res.status(204).send();

    }catch (error){
        next(error);
    }
})

module.exports = WinesController;