//Requerimos mongoose
const mongoose = require('mongoose');
//Utilizamos los esquemas de mongoose
const Schema = mongoose.Schema;

//Inicializamos el nuevo esquema que recibira la db
const wineSchema = new Schema({
        wine: { type: String },
        type: { type: String },
        year: { type: Number },
        winery: { type: String },
        region: { type: String },
        country: { type: String },  
})

////Almacenamos el modelo de esquema en una variable para exportarlo y no perder el auto completado
//El primer parametro correcponde al nombre de la colleción que se asignara en la db y el segundo al schema
const Wines = mongoose.model('Wine', wineSchema)
module.exports = Wines;
