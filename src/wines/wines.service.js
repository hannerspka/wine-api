const CategoryResolver = require("../category/category.resolver");
const StatusError = require("../_shared/error/status.error");
const Wine = require("./wines.model");

class WineService {

    static find() {
        return Wine.find();
    }

    
    static async findOne(id) {
        const wine = await Wine.findById(id);

        if (wine) {
            return wine;
        }

        throw new StatusError(404, `Wine with id <${id}> was not found`);
    }

    static async create(wine) {
         
        return Wine.create(wine);
    }

    static async replace(id, wine) {
        const updateWine = await Wine.findByIdAndUpdate(id, wine);

        if (updateWine) {
            return updateWine;
        }

        throw new StatusError(404, `Wine with id <${id}> was not found`);
    }

    
    static async delete(id) {
        const indexWine = await Wine.findById(id);

        const categories = await CategoryResolver.findByWine(id);
        if (categories.length) {
            throw new StatusError(400, `Wines has linked categories, remove them first before deleting wines`);
        }

        if (indexWine) {
            return Wine.findByIdAndRemove(id);
        }

        throw new StatusError(404, `Wine with id <${id}> was not found`);
    }
}

module.exports = WineService;