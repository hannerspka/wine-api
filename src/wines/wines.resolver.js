const WineService = require("./wines.service");

class WinesResolver {
    static async winesExistsById(id) {
        try {
            await WineService.findOne(id);
            return true;
        } catch (error) {
            throw error;
        }
    }
}

module.exports = WinesResolver;